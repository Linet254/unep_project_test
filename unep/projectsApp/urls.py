
from django.urls import include, path, re_path
from django.conf import settings
from django.conf.urls.static import static
from . import views

from rest_framework import routers
from .api import CountryViewSet, ProjectViewSet, CompletedProjectViewSet, CountryProjectViewSet

router = routers.DefaultRouter()

# API endpoints
router.register('api/countries', CountryViewSet, 'countries'),
router.register('api/projects/all', ProjectViewSet, 'projects'),
router.register('api/projects/country/kenya', CountryProjectViewSet, 'country_projects'),
router.register('api/projects/status/completed', CompletedProjectViewSet, 'completed_projects'),


# urlpatterns = router.urls

urlpatterns = [
    path('', views.index, name='index'),
    path('add/project/', views.add_project, name='add_project'),
    path('edit/project/', views.edit_project, name='edit_project'),
    # path('view/project/', views.view_project, name='view_project'),
    path('delete/project/<int:project_id>', views.delete_project, name='delete_project'),

]+router.urls