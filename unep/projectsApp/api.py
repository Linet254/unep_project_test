from .models import Country, Project
from rest_framework import viewsets, status, generics, permissions, filters
from rest_framework.decorators import action
from rest_framework.response import Response
from .serializers import CountrySerializer, ProjectSerializer

#Lead Viewset
class CountryViewSet(viewsets.ModelViewSet):

	queryset = Country.objects.all()

	permission_classes = [

		permissions.AllowAny

	]

	serializer_class = CountrySerializer



class ProjectViewSet(viewsets.ModelViewSet):

	queryset = Project.objects.all()

	permission_classes = [

		permissions.AllowAny
	]

	serializer_class = ProjectSerializer



class CountryProjectViewSet(viewsets.ModelViewSet):

	queryset = Country.objects.all()

	permission_classes = [

		permissions.AllowAny
	]

	serializer_class = ProjectSerializer

# from rest_framework import filters

# class CountryProjectViewSet(filters.SearchFilter):

#     def get_search_fields(self, view, request):
#         if request.query_params.get('title_only'):
#             return ('country_id',)
#         return super(CountryProjectViewSet, self).get_search_fields(view, request)


class CompletedProjectViewSet(viewsets.ModelViewSet):

	queryset = Project.objects.filter(project_status="C") # fiters the queryset to retain only completed projects

	permission_classes = [

		permissions.AllowAny
	]

	serializer_class = ProjectSerializer



class CompletedProjectViewSet(viewsets.ModelViewSet):

	queryset = Project.objects.all()

	permission_classes = [

		permissions.AllowAny
	]

	serializer_class = ProjectSerializer



	@action(detail=False)
	def completed_projects(self, request):

		completed_projects = Project.objects.filter(project_status = Completed).order_by('-last_login')

		page = self.paginate_queryset(completed_projects)

		if page is not None:

			serializer = self.get_serializer(page, many=True)

			return self.get_paginated_response(serializer.data)

		serializer = self.get_serializer(completed_projects, many=True)

		return Response(serializer.data)


