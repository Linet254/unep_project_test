from django import forms
# from django.contrib.auth.forms import AuthenticationForm
from .models import Project


class ProjectPostForm(forms.ModelForm):
    '''
    Class to create a form for an authenticated user to create Project
    '''
    class Meta:
        model = Project
        # fields = ['title','story', 'country_id']
        fields = '__all__'

class ProjectEditForm(forms.ModelForm):
    '''
    class that creates the comment form
    '''
    class Meta:
        model = Project
        fields = '__all__'
