from django.shortcuts import render

from django.shortcuts import render,redirect
from django.urls import reverse

from django.contrib.auth.models import User
from .models import Country, Project
from .forms import ProjectPostForm, ProjectEditForm

from django.conf import settings





def index(request):

	projects = Project.objects.all()

	return render(request, 'index.html', {"projects":projects})


def add_project(request):
    '''
    View function to display a form for creating a project
    '''

    if request.method == 'POST':

        form = ProjectPostForm(request.POST, request.FILES)

        if form.is_valid:
            post = form.save(commit=False)
            post.save()
            return redirect(profile)
    else:
        form = ProjectPostForm()
    return render(request, 'new-project.html', {"form":form})



def edit_project(request):


    if request.method == 'POST':

        form = ProjectPostForm(request.POST, request.FILES)

        if form.is_valid:
            post = form.save(commit=False)
            post.save()
            return redirect(profile)
    else:
        form = ProjectPostForm()
    return render(request, 'edit-project.html', {"form":form})



def delete_project(request, project_id):

	project = Project.objects.get(pk=project_id)

	project.delete()

	return redirect(index)



ded view_project(request, project_id):

	project = Project.objects.get(pk=project_id)

	return render(request, 'view-project.html', {"project":project})