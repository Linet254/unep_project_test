from rest_framework import serializers
from projectsApp.models import Country, Project




#Country Serializer
class CountrySerializer(serializers.ModelSerializer):

    class Meta:

        model = Country

        fields = '__all__' # serialize all the fields of the Countries table
        




class ProjectSerializer(serializers.ModelSerializer):

    class Meta:

        model = Project

        fields = '__all__' # serialize all the fields of the Project table      