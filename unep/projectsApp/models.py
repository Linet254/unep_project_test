from django.db import models


class Country(models.Model):
	""" This class creates the record for a country

	"""
	DEVELOPEMENT_CATEGORY_CHOICES = (

        ('D', 'Developing Country'),

        ('L', 'Least Developed Country'),

    )

	country_name = models.CharField(max_length=100)

	region = models.CharField(max_length=100)

	country_classification = models.CharField(max_length=1, choices=DEVELOPEMENT_CATEGORY_CHOICES, null=True, default='D') # on creation of a country record in the db, it's classified as a developing country by default


	# Ensure gramatically sound naming country table in the admin panael (ie plural of country as countries, not countrys)
	# class Meta:

	# 	verbose_name = _("Country")

	# 	verbose_name_plural = _("Countries")

	# 	ordering = ("country_name")





	def __str__(self):
		"""
			Initialize the class and display a human readable  country_name
		"""
		return self.country_name

	@classmethod
	def create_country(cls,self):
		pass
	










class Project(models.Model):

	PROJECT_STATUS_CHOICES = (

	    ('C', 'Complete'),

	    ('I', 'Under Implementation'),

	)

	country_id = models.ForeignKey(Country, null=True, blank=True, on_delete=models.CASCADE)

	title = models.TextField(max_length=10000)

	objectives = models.TextField(max_length=10000, null=True, blank=True)

	party_to = models.TextField(max_length=10000, null=True, blank=True)

	duration = models.CharField(max_length=100, null=True, blank=True)

	story = models.TextField(max_length=30000, null=True, blank=True)

	special_program_trust_fund = models.CharField(max_length=100, null=True, blank=True)
	
	co_financing = models.CharField(max_length=100, null=True, blank=True)

	project_status = models.CharField(max_length=1, choices=PROJECT_STATUS_CHOICES, null=True, default='I') # on creation of a project record in the db, it is marked as under impelentation by default





